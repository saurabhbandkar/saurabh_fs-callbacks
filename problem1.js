const fs = require('fs');
const createJsonFiles = function (noOfFiles, callback) {
    let path = './jsonDirectory';

    if (noOfFiles > 0) {
        const names = ['Rahul', 'Sachin', 'Sourav', 'Kumble', 'Kohli', 'Bumrah', 'Rohit', 'Ravindra', 'Dhoni'];
        const tShirtNo = [1, 2, 3, 4, 5, 6, 7, 8, 9];
        const attribute = ['Batsmen', 'Bowler', 'Allrounder', 'Wicket keeper'];
        const player = new Object();

        const randomize = function (array) {
            return Math.floor((Math.random() * array.length));
        };

        const fileCount = Array.from({ length: noOfFiles }, (value, index) => {
            return index + 1;
        });

        const playersList = fileCount.map((currentValue) => {

            let fileName = path + '/JSONFile' + currentValue + '.json';

            player['name'] = names[randomize(names)];
            player['tShirtNo'] = tShirtNo[randomize(tShirtNo)];
            player['attribute'] = attribute[randomize(attribute)];

            let outputData = JSON.stringify(player);

            fs.writeFile(fileName, outputData, 'utf-8', (err) => {
                if (err) {
                    console.error(err);
                }
                else {
                    console.log(`File ${currentValue} saved successfully!!`);
                    console.log(`File ${currentValue} data : ` + outputData + '\n');
                };
            });
        });
        callback(null, path);
    }
    else {
        callback(new Error(' No. of files required is incorrect'), null);
    };
};

const deleteJsonFiles = function (path, callback) {
    if (!typeof (path) == 'string') {
        callback(err);
    }
    else {
        setTimeout(() => {
            fs.readdir(path, 'utf-8', (err, files) => {
                if (err) {
                    console.error(err);
                }
                else {
                    filesList = files;
                    callback(null, filesList);
                    setTimeout(() => {
                        filesList.map((currentValue) => {
                            let fileName = path + '/' + currentValue;
                            fs.unlink(fileName, (err, data) => {
                                if (err) {
                                    console.error(err);
                                }
                                else {
                                    console.log(`File ${currentValue} deleted successfully!!!`);
                                };
                            });
                        });
                    }, 0);
                };
            });
        }, 5 * 1000);
    };
};


const createThenDeleteFiles = function (noOfFiles, callback) {
    if ((typeof (noOfFiles) == 'number') && (typeof (callback) == 'function')) {

        let filesList = new Array();
        let path = './jsonDirectory';

        fs.mkdir(path, (err) => {
            if (err) {
                console.log('Folder exists, hence new folder not created.');
                console.log(`Files will be deleted in 5 seconds!!`);
                callback(null, noOfFiles);
            }
            else {
                console.log('New Folder created');
                callback(null, noOfFiles);
                console.log(`Files will be deleted in 5 seconds!!`);
            };
        });
    }
    else {
        callback(new Error('Incorrect arguments provided!!'), null);
    };
};


module.exports = { createThenDeleteFiles, deleteJsonFiles, createJsonFiles };
