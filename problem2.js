const fs = require('fs');
const fileName = './lipsum.txt';
const path = './';

const readInputFileName = function (fileName, callback) {
    if (typeof (fileName) == 'string') {
        fs.readFile(fileName, 'utf-8', (err, data) => {
            if (err) {
                console.error(err);
            }
            else {
                console.log('Reading file success');
                callback(null, data);
            };
        });
    }
    else {
        callback(new Error('Error reading input file'), null);
    };
};


const upperCase = function (data, callback) {
    if (typeof (data) == 'string') {
        let fileName = 'upperCaseData.txt';
        const upperCaseData = data.toUpperCase();
        fs.writeFile(fileName, upperCaseData, 'utf-8', (err) => {
            if (err) {
                console.error(err);
            }
            else {
                console.log('Data converted to upper case');
                saveFile(fileName, (err) => {
                    if (err) {
                        console.log(err);
                    };
                });
                callback(null, data);
            };
        });
    }
    else {
        callback(new Error('uppercase error'), null);
    };
};

const lowerCase = function (fileName, callback) {
    readInputFileName(fileName, (err, data) => {
        if (err) {
            console.error(err);
        }
        else {
            let fileName = 'lowerCaseData.txt';
            const lowerCaseData = data.toLowerCase();
            const sentenceForm = lowerCaseData.split('. ').join('.\n');
            fs.writeFile(fileName, sentenceForm, 'utf-8', (err) => {
                if (err) {
                    console.error(err);
                }
                else {
                    console.log('Data converted to lower case');
                    saveFile(fileName, (err) => {
                        if (err) {
                            console.error(err);
                        };
                    });
                    callback(null, data);
                };
            });
        };
    });
};


const doSort = function (fileName, callback) {
    if (typeof (fileName) == 'string') {
        readInputFileName(fileName, (err, data) => {
            if (err) {
                console.error(err);
            }
            else {
                let fileName = 'sortedData.txt';
                const dataArray = data.split('.\n');
                const sortedDataArray = dataArray.sort();
                const sortedData = sortedDataArray.join('.\n');

                fs.writeFile(fileName, sortedData, 'utf-8', (err) => {
                    if (err) {
                        console.error(err);
                    }
                    else {
                        console.log('Data is now sorted');
                        saveFile(fileName, (err) => {
                            if (err) {
                                console.error(err);
                            };
                        });
                        callback(null, data);
                    };
                });
            };
        });
    }
    else {
        callback(new Error('error in sorting'), null);
    };
};


const deleteAllFiles = function (fileName, callback) {
    if ((typeof (fileName) == 'string') && (typeof (callback) == 'function')) {
        readInputFileName(fileName, (err, filesList) => {
            if (err) {
                console.error(err);
            }
            else {
                setTimeout(() => {
                    filesList = filesList.split('\n');
                    filesList.map((currentValue) => {
                        if (!currentValue == '') {
                            let fileName = path + '/' + currentValue;
                            fs.unlink(fileName, (err, data) => {
                                if (err) {
                                    console.error(err);
                                }
                                else {
                                    console.log(`File ${currentValue} deleted successfully!!!`);
                                };
                            });
                        };
                    });
                }, 0);
                callback(null, filesList);
            };
        });
    }
    else {
        callback(new Error('Error deleting files'), null);
    };
};

const saveFile = function (content, callback) {
    if (typeof (content) == 'string') {
        fs.appendFile('fileNames.txt', content + '\n', (err) => {
            if (err) {
                console.error(err);
            }
            else {
                console.log('Data saved to file.');
            };
        });
    }
    else {
        callback(new Error('File not Saved!!!'));
    };
};


module.exports = { saveFile, deleteAllFiles, doSort, lowerCase, upperCase, readInputFileName };