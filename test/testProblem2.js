const  { saveFile, deleteAllFiles, doSort, lowerCase, upperCase, readInputFileName } = require ('../problem2');
const fileName = '../lipsum.txt';
const fs = require('fs');
const path = '../';

readInputFileName(fileName, (err, data) => {
    if (err) {
        console.error(err);
    }
    else {
        upperCase(data, (err, data) => {
            if (err) {
                console.error(err);
            }
            else {
                lowerCase('upperCaseData.txt', (err, data) => {
                    if (err) {
                        console.error(err);
                    }
                    else {
                        doSort('lowerCaseData.txt', (err, data) => {
                            if (err) {
                                console.error(err);
                            }
                            else {
                                deleteAllFiles('fileNames.txt', (err, data) => {
                                    if (err) {
                                        console.error(err);
                                    }
                                    else {
                                        console.log('All operations completed');
                                    };
                                });
                            };
                        });
                    };
                });
            };
        });
    };
});
