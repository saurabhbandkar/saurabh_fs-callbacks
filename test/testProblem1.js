const { createThenDeleteFiles, deleteJsonFiles, createJsonFiles } = require('../problem1');
const fs = require('fs');

createThenDeleteFiles(5, (err, noOfFiles) => {
    if (err) {
        console.error(err);
    }
    else {

        createJsonFiles(noOfFiles, (err, fileCountData) => {
            if (err) {
                console.error(err);
            }
            else {
                deleteJsonFiles(fileCountData, (err)=>{
                    if (err) {
                        console.error (err);
                    }
                    else {
                        console.log ('All tasks completed');
                    };
                });
            };
        });
    };
});
